import dj_database_url
from decouple import config

from shopping_cart.settings.base import *


DEBUG = True

ALLOWED_HOSTS = ['*', ]

DATABASES = {
    'default': dj_database_url.config(
        default=config('DATABASE_URL')
    )
}

STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'
