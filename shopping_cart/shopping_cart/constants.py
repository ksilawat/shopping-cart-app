ADMIN = 'admin'
SHOP_OWNER = 'shop_owner'
CUSTOMER = 'customer'

ROLES = (
    (ADMIN, 'Admin'),
    (SHOP_OWNER, 'Shop Owner'),
    (CUSTOMER, 'Customer'),
)

FIRST_NAME_MAX_LENGTH = 20
LAST_NAME_MAX_LENGTH = 20

ACCEPTED = 'accepted'
REJECTED = 'rejected'
NOT_VISITED = 'not_visited'

SHOPREQUEST_STATUSES = (
	(ACCEPTED, 'Accepted'),
	(REJECTED, 'Rejected'),
	(NOT_VISITED, 'Not Visited'),
)
