"""shopping_cart URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf import settings
from django.urls import path, include
from django.conf.urls.static import static

from core.views import HomeView
from user.views import \
    CustomerSignupView, \
    CustomerLoginView, \
    ShopOwnerSignupView, \
    ShopOwnerLoginView, \
    CustomerDashBoardView, \
    ShopOwnerDashBoardView, \
    EditShopDetailsView
from product.views import \
    AddProductCategoryView, \
    AddProductView, \
    PublishProductView, \
    UnPublishProductView, \
    EditProductView, \
    DeleteProductView, \
    UpdateProductStockView


urlpatterns = [
    path('', HomeView.as_view(), name='home'),
    path('customer/signup/', CustomerSignupView.as_view(), name='customer_signup'),
    path('customer/login/', CustomerLoginView.as_view(), name='customer_login'),
    path('shop_owner/signup/', ShopOwnerSignupView.as_view(), name='shop_owner_signup'),
    path('shop_owner/login/', ShopOwnerLoginView.as_view(), name='shop_owner_login'),
    path('customer/dashboard/', CustomerDashBoardView.as_view(), name='customer_dashboard'),
    path('shop_owner/dashboard/', ShopOwnerDashBoardView.as_view(), name='shop_owner_dashboard'),
    path('edit_shop/<int:pk>', EditShopDetailsView.as_view(), name='edit_shop'),
    path('add_product_category/', AddProductCategoryView.as_view(), name='add_product_category'),
    path('add_product/', AddProductView.as_view(), name='add_product'),
    path('edit_product/<int:pk>', EditProductView.as_view(), name='edit_product'),
    path('delete_product/<int:pk>', DeleteProductView.as_view(), name='delete_product'),
    path('publish_product/<int:pk>', PublishProductView.as_view(), name='publish_product'),
    path('unpublish_product/<int:pk>', UnPublishProductView.as_view(), name='unpublish_product'),
    path('update_product_stock/<int:pk>', UpdateProductStockView.as_view(), name='update_product_stock'),
    path('admin/', admin.site.urls),
    path('accounts/', include('allauth.urls'))
] + static(
    settings.STATIC_URL, document_root=settings.STATIC_ROOT
) + static(
    settings.MEDIA_URL, document_root=settings.MEDIA_ROOT
)
