from django.utils import timezone
from django.contrib import admin
from django.contrib.sites.shortcuts import get_current_site

from shopping_cart.constants import ACCEPTED, REJECTED
from user.models import \
    User, \
    Address, \
    Shop
from user.utils import send_review_status


class AddressInline(admin.TabularInline):

    model = Address

    extra = 0


class UserAdmin(admin.ModelAdmin):

    inlines = (
        AddressInline,
    )

    list_display = (
        'username',
        'email',
        'first_name',
        'last_name',
        'role',
    )


class ShopAdmin(admin.ModelAdmin):

    list_display = (
        'owner',
        'status',
        'reviewer',
    )

    def save_model(self, request, obj, form, change):
        if change and form.has_changed() \
            and 'status' in form.changed_data \
            and (form.cleaned_data.get('status') == ACCEPTED \
            or form.cleaned_data.get('status') == REJECTED):
            obj.reviewed_by = request.user
            obj.reviewed_at = timezone.now()
            send_review_status(
                site=get_current_site(request),
                owner_email=obj.owner.email,
                status=form.cleaned_data.get('status')
            )
        super(ShopAdmin, self).save_model(request, obj, form, change)


admin.site.register(User, UserAdmin)
admin.site.register(Shop, ShopAdmin)
