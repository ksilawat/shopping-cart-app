from django.urls import reverse

from allauth.account.adapter import DefaultAccountAdapter


class CustomAdapter(DefaultAccountAdapter):

    def get_email_confirmation_redirect_url(self, request):
        return reverse('home')
