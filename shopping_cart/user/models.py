from django.db import models
from django.conf import settings
from django.contrib.auth.models import AbstractUser

from shopping_cart.constants import \
    ROLES, \
    ADMIN, \
    SHOPREQUEST_STATUSES, \
    NOT_VISITED


class User(AbstractUser):

    role = models.CharField(
        max_length=10,
        choices=ROLES,
        default=ADMIN
    )


class Address(models.Model):

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name='addresses',
    )

    street_address_1 = models.CharField(
        max_length=100,
    )

    street_address_2 = models.CharField(
        max_length=100,
        blank=True,
    )

    city = models.CharField(
        max_length=50,
    )

    postal_code = models.CharField(
        max_length=20,
    )


class Shop(models.Model):

    name = models.CharField(
        max_length=128,
        null=True,
        blank=True,
    )

    owner = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name='submitted_by'
    )

    submitted_at = models.DateTimeField(
        auto_now=True,
    )

    status = models.CharField(
        max_length=15,
        choices=SHOPREQUEST_STATUSES,
        default=NOT_VISITED,
    )

    reviewer = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        on_delete=models.DO_NOTHING,
        related_name='approved_by',
    )

    reviewed_at = models.DateTimeField(
        null=True,
        blank=True,
    )
