from celery import shared_task

from user.utils import send_mails_to_admins


@shared_task
def requests_review_alert():
    send_mails_to_admins(
        subject='Shop Registrations Review Alert',
        message='Please check for new Shop-Request Registerations.',
    )
