from django.conf import settings
from django.urls import reverse, reverse_lazy
from django.views.generic import UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.sites.shortcuts import get_current_site

from search_listview.list import SearchableListView

from allauth.account.views import \
    SignupView, \
    LoginView
from allauth.exceptions import ImmediateHttpResponse

from user.forms import SignupForm
from user.models import Shop
from user.utils import send_mails_to_admins
from product.models import Product


class BaseSignupView(SignupView):

    template_name = 'user/signup.html'

    form_class = SignupForm


class CustomerSignupView(BaseSignupView):

    def get_context_data(self, **kwargs):
        context = super(BaseSignupView, self).get_context_data(**kwargs)
        context.update(
            {
                'role': 'Customer',
                'login_url': reverse('customer_login'),
                'signup_url': reverse('customer_signup'),
            }
        )
        return context


class ShopOwnerSignupView(BaseSignupView):

    def get_context_data(self, **kwargs):
        context = super(BaseSignupView, self).get_context_data(**kwargs)
        context.update(
            {
                'role': 'Shop Owner',
                'login_url': reverse('shop_owner_login'),
                'signup_url': reverse('shop_owner_signup'),
            }
        )
        return context

    def form_valid(self, form):
        try:
            response = super(ShopOwnerSignupView, self).form_valid(form)
            Shop.objects.get_or_create(
                owner=self.user,
            )
            return response
        except ImmediateHttpResponse as e:
            return e.response


class BaseLoginView(LoginView):

    template_name = 'user/login.html'


class CustomerLoginView(BaseLoginView):

    def get_success_url(self):
        return reverse('customer_dashboard')

    def get_context_data(self, **kwargs):
        context = super(BaseLoginView, self).get_context_data(**kwargs)
        context.update(
            {
                'role': 'Customer',
                'login_url': reverse('customer_login'),
                'signup_url': reverse('customer_signup'),
            }
        )
        return context


class ShopOwnerLoginView(BaseLoginView):

    def get_success_url(self):
        return reverse('shop_owner_dashboard')

    def get_context_data(self, **kwargs):
        context = super(BaseLoginView, self).get_context_data(**kwargs)
        context.update(
            {
                'role': 'Shop Owner',
                'login_url': reverse('shop_owner_login'),
                'signup_url': reverse('shop_owner_signup'),
            }
        )
        return context


class CustomerDashBoardView(LoginRequiredMixin, SearchableListView):

    login_url = reverse_lazy('customer_login')

    model = Product
    paginate_by = 12
    template_name = 'user/customer_dashboard.html'
    searchable_fields = ["title", ]
    specifications = {
        "title": "__icontains",
    }


class ShopOwnerDashBoardView(LoginRequiredMixin, SearchableListView):

    login_url = reverse_lazy('shop_owner_login')

    model = Product
    paginate_by = 12
    template_name = 'user/shop_owner_dashboard.html'
    searchable_fields = ["title", ]
    specifications = {
        "title": "__icontains",
    }

    def get_queryset(self):
        shop = Shop.objects.get(owner=self.request.user)
        queryset = self.model.objects.filter(
            sold_by=shop,
        )
        return queryset

    def get_context_data(self, **kwargs):
        context = super(ShopOwnerDashBoardView, self).get_context_data(**kwargs)
        shop = Shop.objects.get(owner=self.request.user)
        context.update(
            {
                'shop_pk': shop.pk,
            }
        )
        return context


class EditShopDetailsView(LoginRequiredMixin, UpdateView):

    login_url = reverse_lazy('shop_owner_login')

    model = Shop
    fields = (
        'name',
    )
    template_name = 'user/edit_shop_details.html'
