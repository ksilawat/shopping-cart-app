from django.dispatch import receiver
from django.contrib.auth import get_user_model

from allauth.account import signals


User = get_user_model()

@receiver(signals.user_signed_up, sender=User)
def generate_shop_request(sender, **kwargs):
    pass
