# Generated by Django 3.1.7 on 2021-04-01 10:36

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0005_auto_20210401_0818'),
    ]

    operations = [
        migrations.RenameField(
            model_name='shoprequest',
            old_name='approved_at',
            new_name='reviewed_at',
        ),
        migrations.RenameField(
            model_name='shoprequest',
            old_name='approved_by',
            new_name='reviewed_by',
        ),
    ]
