from django import forms
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

from allauth.account.forms import SignupForm

from shopping_cart.constants import \
    SHOP_OWNER, \
    CUSTOMER, \
    FIRST_NAME_MAX_LENGTH, \
    LAST_NAME_MAX_LENGTH


class SignupForm(SignupForm):

    first_name = forms.CharField(
        label=_("First Name"),
        max_length=FIRST_NAME_MAX_LENGTH,
        widget=forms.TextInput(
            attrs={"placeholder": _("First Name"), "autocomplete": "first_name"}
        ),
    )

    last_name = forms.CharField(
        label=_("Last Name"),
        max_length=LAST_NAME_MAX_LENGTH,
        widget=forms.TextInput(
            attrs={"placeholder": _("Last Name"), "autocomplete": "last_name"}
        ),
    )

    def save(self, request):
        user = super(SignupForm, self).save(request)
        user.role = SHOP_OWNER
        if request.path == reverse('customer_signup'):
            user.role = CUSTOMER
        user.save()
        return user
