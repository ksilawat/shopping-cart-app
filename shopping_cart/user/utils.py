from django.core.mail import send_mail
from django.contrib.auth import get_user_model
from django.template.loader import render_to_string
# from django.http import HttpResponseRedirect

# from allauth.account import signals
# from allauth.account.adapter import get_adapter
# from allauth.account.utils import \
#     _has_verified_for_login, \
#     send_email_confirmation
# from allauth.account.app_settings import EmailVerificationMethod

from shopping_cart.constants import ADMIN


User = get_user_model()


# def complete_signup(request, user, email_verification, success_url, signal_kwargs=None):
#     if signal_kwargs is None:
#         signal_kwargs = {}
#     signals.user_signed_up.send(
#         sender=user.__class__, request=request, user=user, **signal_kwargs
#     )
#     signup = True
#     email = None
#     adapter = get_adapter(request)
#     if not user.is_active:
#         return adapter.respond_user_inactive(request, user)

#     if email_verification == EmailVerificationMethod.NONE:
#         pass
#     elif email_verification == EmailVerificationMethod.OPTIONAL:
#         if not _has_verified_for_login(user, email) and signup:
#             send_email_confirmation(request, user, signup=signup, email=email)
#     elif email_verification == EmailVerificationMethod.MANDATORY:
#         if not _has_verified_for_login(user, email):
#             send_email_confirmation(request, user, signup=signup, email=email)
#             return adapter.respond_email_verification_sent(request, user)
#     response = HttpResponseRedirect(reverse(redirect_url))


def send_mails_to_admins(subject, message):
    to = list(User.objects.filter(
            role=ADMIN,
        ).values_list(
            'email',
            flat=True
        ).distinct())

    send_mail(
        subject=subject,
        message=message,
        from_email=None,
        recipient_list=to
    )


def send_review_status(site, owner_email, status):
    template = 'user/email/review_status.txt'
    context = {
        'current_site': site,
        'status': status,
    }
    message = render_to_string(template, context)

    send_mail(
        subject='Shop Request review status',
        message=message,
        from_email=None,
        recipient_list=[owner_email],
    )
