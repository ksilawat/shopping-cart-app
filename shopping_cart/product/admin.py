from django.contrib import admin


from product.models import \
    ProductCategory, \
    Product


class ProductCategoryAdmin(admin.ModelAdmin):

    list_display = (
        'name',
    )


class ProductAdmin(admin.ModelAdmin):

    list_display = (
        'title',
        'date_created',
        'date_updated',
        'category',
    )


admin.site.register(ProductCategory, ProductCategoryAdmin)
admin.site.register(Product, ProductAdmin)
