from django.urls import reverse_lazy
from django.views.generic import \
    CreateView, \
    RedirectView, \
    UpdateView, \
    DeleteView
from django.http import HttpResponseRedirect
from django.contrib.auth.mixins import LoginRequiredMixin

from user.models import Shop
from product.models import \
    ProductCategory, \
    Product, \
    ProductStock, \
    ProductStockRecord


class AddProductCategoryView(LoginRequiredMixin, CreateView):

    login_url = reverse_lazy('shop_owner_login')

    model = ProductCategory
    fields = '__all__'
    success_url = reverse_lazy('add_product')
    template_name = 'product/add_product_category.html'


class AddProductView(LoginRequiredMixin, CreateView):

    login_url = reverse_lazy('shop_owner_login')

    model = Product
    fields = (
        'title',
        'description',
        'category',
        'image'
    )
    success_url = reverse_lazy('shop_owner_dashboard')
    template_name = 'product/add_product.html'

    def form_valid(self, form):
        self.object = form.save(commit=False)
        shop = Shop.objects.get(owner=self.request.user)
        self.object.sold_by = shop
        self.object.save()
        return HttpResponseRedirect(self.get_success_url())


class EditProductView(LoginRequiredMixin, UpdateView):

    login_url = reverse_lazy('shop_owner_login')

    model = Product
    fields = (
        'title',
        'description',
        'image'
    )
    success_url = reverse_lazy('shop_owner_dashboard')
    template_name = 'product/edit_product.html'


class DeleteProductView(LoginRequiredMixin, DeleteView):

    login_url = reverse_lazy('shop_owner_login')

    model = Product
    success_url = reverse_lazy('shop_owner_dashboard')

    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)


class BasePublishProductView(LoginRequiredMixin, RedirectView):

    login_url = reverse_lazy('shop_owner_login')

    url = reverse_lazy('shop_owner_dashboard')


class PublishProductView(BasePublishProductView):

    def get(self, request, *args, **kwargs):
        pk = kwargs.get('pk')
        product = Product.objects.get(pk=pk)
        product.publish = True
        product.save(update_fields=['publish', ])
        return super(PublishProductView, self).get(request, *args, **kwargs)


class UnPublishProductView(BasePublishProductView):

    def get(self, request, *args, **kwargs):
        pk = kwargs.get('pk')
        product = Product.objects.get(pk=pk)
        product.publish = False
        product.save(update_fields=['publish', ])
        return super(UnPublishProductView, self).get(request, *args, **kwargs)


class UpdateProductStockView(LoginRequiredMixin, UpdateView):

    login_url = reverse_lazy('shop_owner_login')

    model = ProductStock
    fields = (
        'num_in_stock',
        'price',
        'low_stock_threshold'
    )
    success_url = reverse_lazy('shop_owner_dashboard')
    template_name = 'product/update_productstock.html'

    def form_valid(self, form):
        self.object = form.save()
        stock_record = ProductStockRecord(
            stock=self.object,
            alloted_stock=self.object.num_in_stock,
            stock_left=(self.object.num_in_stock - self.object.num_allocated),
            price=self.object.price
        )
        stock_record.save()
        return HttpResponseRedirect(self.get_success_url())
