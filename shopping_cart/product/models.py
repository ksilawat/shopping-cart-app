from django.db import models

from user.models import Shop


class ProductCategory(models.Model):

    name = models.CharField(
        max_length=128,
    )

    description = models.TextField(
        null=True,
        blank=True,
    )

    def __str__(self):
        return self.name


class Product(models.Model):

    title = models.CharField(
        max_length=128,
    )

    description = models.TextField(
        null=True,
        blank=True,
    )

    date_created = models.DateTimeField(
        auto_now_add=True,
    )

    date_updated = models.DateTimeField(
        auto_now=True,
    )

    publish = models.BooleanField(
        default=False,
    )

    image = models.ImageField(
        null=True,
        blank=True,
        upload_to='product_images',
    )

    category = models.ForeignKey(
        ProductCategory,
        related_name='products',
        on_delete=models.CASCADE,
    )

    sold_by = models.ForeignKey(
        Shop,
        related_name='products',
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        super(Product, self).save(*args, **kwargs)
        ProductStock.objects.get_or_create(
            product=self,
        )



class ProductStock(models.Model):

    product = models.OneToOneField(
        Product,
        related_name='stock',
        on_delete=models.CASCADE,
    )

    num_in_stock = models.PositiveIntegerField(
        default=0,
    )

    num_allocated = models.PositiveIntegerField(
        default=0,
    )

    price = models.DecimalField(
        default=0,
        max_digits=12,
        decimal_places=2,
    )

    low_stock_threshold = models.PositiveIntegerField(
        default=0,
    )

    date_created = models.DateTimeField(
        auto_now_add=True,
    )

    date_updated = models.DateTimeField(
        auto_now=True,
    )

    def __str__(self):
        return f'Stock for: {self.product}'


class ProductStockRecord(models.Model):

    stock = models.ForeignKey(
        ProductStock,
        related_name='stockrecords',
        on_delete=models.CASCADE,
    )

    alloted_stock = models.PositiveIntegerField(
        default=0,
    )

    stock_left = models.PositiveIntegerField(
        default=0,
    )

    price = models.DecimalField(
        default=0,
        max_digits=12,
        decimal_places=2,
    )

    date_created = models.DateTimeField(
        auto_now_add=True,
    )

    def __str__(self):
        return f'Stock Record for: {self.stock.product}'
