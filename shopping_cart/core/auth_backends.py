from django import forms
from django.contrib.auth import get_user_model

from allauth.account.auth_backends import AuthenticationBackend

from user.models import Shop
from shopping_cart.constants import SHOP_OWNER, ACCEPTED


User = get_user_model()

class CustomAuthenticationBackend(AuthenticationBackend):

    def authenticate(self, request, **credentials):
        ret = super(CustomAuthenticationBackend, self).authenticate(request, **credentials)

        if isinstance(ret, User) and ret.role == SHOP_OWNER:
            shop = Shop.objects.get(owner=ret)
            if shop.status == ACCEPTED:
                return ret
            else:
                raise forms.ValidationError(
                    "Shop registeration request not accepted yet."
                )
        return ret
